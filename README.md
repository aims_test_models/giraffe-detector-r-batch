# r_batch_test

A short description of the project.



## Artificial Intelligence Management Service Project Template

This is the AIMS project template. 



## Track parameters, metrics and artifacts during training

### Python

In python you can track parameters, metrics and artifacts by using the [Sacred library](https://github.com/IDSIA/sacred). 
The tracked data is stored in the repository and can be visualized in the AIMS user interface after it got pushed to git.

There is a simple example in the train.py file. For further information take a look at the [Sacred docs](https://sacred.readthedocs.io/en/latest/quickstart.html).


## Get your repository ready for deployment.
You should define the needed dependencies in either the `requirements.txt` for a python environment or 
the `install_requirements.R` for an R environment. On deployment a new environment will be setup with the defined 
dependencies.


Also you should add code to the `process.py` or `process.R` files to define how the model will be loaded and setup
 for inference.